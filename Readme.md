<h4> How to: </h4>
<p>To use the this project, import it as an existing Maven project.</p>
<p>Errors in the servicetracker package can be resolved by adding the dependencies to the build path.</p>
<p>In Eclipse this can be done by highlighting the error, selecting "fix project settings" and confirming the changes.</p>