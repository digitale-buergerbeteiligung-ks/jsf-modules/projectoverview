package servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import com.liferay.document.library.kernel.service.DLAppLocalService;

public class DLAppLocalServiceTracker extends ServiceTracker<DLAppLocalService,DLAppLocalService>{
    public DLAppLocalServiceTracker(BundleContext bundleContext){
        super(bundleContext, DLAppLocalService.class, null);
    }
}
